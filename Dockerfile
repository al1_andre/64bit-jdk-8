FROM ubuntu:xenial

MAINTAINER Alain ANDRE <dev@alain-andre.fr>

ENV DEBIAN_FRONTEND noninteractive

# Jdk
RUN apt-get update && apt-get install -y wget git curl zip software-properties-common && \
    add-apt-repository ppa:openjdk-r/ppa && \
    apt-get update && apt-get install -y openjdk-8-jdk && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV JAVA_TOOL_OPTIONS "-Dfile.encoding=UTF8"
